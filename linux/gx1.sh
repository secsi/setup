cat <<EOF >> ~/.bashrc
alias ll="ls --color=always -lhaG"
alias l='ll'
alias f="find . -name "
alias ev="vim ~/.vim_runtime/my_configs.vim"
alias eb="vim ~/.bashrc && source ~/.bashrc"
alias g="git"
alias es="vim ~/.ssh/config"
alias xclip="xclip -selection c"
alias e="exit"
alias h="history"
# alias v="vagrant" 
alias v="vim" 
alias m="make"
export EDITOR=vim

EOF


git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh


cat <<EOF >> ~/.vim_runtime/my_configs.vim  
set nu
 
 
noremap <silent> <C-S>          :update<CR>
vnoremap <silent> <C-S>         <C-C>:update<CR>
inoremap <silent> <C-S>         <C-O>:update<CR>
 
 
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab  
EOF  


cat <<EOF >> ~/.tmux.conf   

 # remap prefix to Control + a
 set -g prefix C-a
 # # bind 'C-a C-a' to type 'C-a'
 bind C-a send-prefix
 unbind C-b

 bind -n M-Left select-pane -L
 bind -n M-Right select-pane -R
 bind -n M-Up select-pane -U
 bind -n M-Down select-pane -D

 set -g mouse off

 #source ~/.bashrc
 bind D source-file ~/.tmux/dev
 set -g default-terminal "screen-256color"

EOF